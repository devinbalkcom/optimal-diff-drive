import matplotlib
import matplotlib.pyplot as pyplot
import numpy as np

from optimal_diff_drive import *

def compute_grid(fn, bounds=((0, 1), (0, 1)), delta=.01):
    xrange = np.arange(bounds[0][0], bounds[0][1], delta)
    yrange = np.arange(bounds[1][0], bounds[1][1], delta)

    xgrid, ygrid = np.meshgrid(xrange, yrange)

    # vectorize fn so it can be applied over the meshgrid

    vfn = np.vectorize(fn, otypes=[np.float64])

    zgrid = vfn(xgrid, ygrid)

    return xgrid, ygrid, zgrid


def plot_surface_map(title, height_function, bounds=((0, 1), (0, 1))):
    xgrid, ygrid, zgrid = compute_grid(height_function, bounds)

    pyplot.figure()
    pyplot.contour(xgrid, ygrid, zgrid, 20)

    pyplot.title(title)
    pyplot.axis('equal')
    pyplot.colorbar()


def plotfn_dd45_tst(x, y):
    return value_base_tst(x, y, pi/4)


def plotfn_dd45(x, y):
    return opt_bvdd(x, y, pi/4)

plot_surface_map("", plotfn_dd45, bounds=((-2, 2), (-2, 2)))
#plot_surface_map("", angle_cost, bounds=((-pi, pi), (-pi, pi)))
pyplot.show()
#print(plotfn_dd45(1, 0))
