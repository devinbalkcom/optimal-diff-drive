from math import sqrt, atan2, acos, sin, cos, tan, pi


def T1(x, y, theta):
    return -x, -y, theta


def T2(x, y, theta):
    ct = cos(theta)
    st = sin(theta)

    # rotate (x, -y) by theta
    nx = ct * x - st * -y
    ny = st * x + ct * -y

    return nx, ny, theta


def T3(x, y, theta):
    return x, -y, -theta


# compute angular cost using angles relative to angle of line
# between start and goal

def angle_cost(a_s, a_g):
    cost_reverse = abs(a_s) + abs(a_g)
    cost_forward = abs(pi - abs(a_s)) + abs(pi - abs(a_g))

    return min(cost_forward, cost_reverse)


def value_base_tst(x, y, theta):
    r = sqrt(x * x + y * y)
    phi = atan2(y, x)

    # angles relative to line between start and goal
    a_s = theta - phi
    a_g = -phi

    # factor of 1/2 due to turning speed that depends on wheelbase
    turn_cost = .5 * angle_cost(a_s, a_g)

    total_cost = r + turn_cost

    return total_cost


def value_base_sts(x, y, theta):
    if y == 0:
        return abs(x) + theta / 2
    return y * (1 + cos(theta))/sin(theta) - x + theta/2


def value_base_tsts(x, y, theta):

    #print(theta, acos(1 - y), -theta / 2)
    return acos(1 - y) - theta / 2 - x + sqrt(y * (2 - y))


def opt_bvdd(x, y, theta):
    if theta > pi and theta < 2 * pi:
        return opt_bvdd(*T3(x, y, theta))
        #return 1

    r = sqrt(x * x + y * y)
    phi = atan2(y, x)

    if (phi > (theta + pi) / 2 and phi < pi) or \
        (phi > (theta - pi) / 2 and phi < 0 ):
        return opt_bvdd(*T2(x, y, theta))
        #return 2

    if y < 0:
        return opt_bvdd(*T1(x, y, theta))

    if phi <= theta:
        return value_base_tst(x, y, theta)
        #return 3
    elif y <= 1 - cos(theta):
        return value_base_sts(x, y, theta)
        #return 4
    elif r > tan(phi / 2):
        return value_base_tst(x, y, theta)
        #return 5
    else:
        return value_base_tsts(x, y, theta)
        #return 6



#value = value_base_tst(1, 0, .1)
#print(value)
